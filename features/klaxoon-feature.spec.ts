import 'testcafe';
import { getCurrentConfig } from '../config/testcafe-config';
import { pageModel } from '../models';
import { env } from '../step-filters/env';
import { and, given, then, when } from '../step-runner';

/**
 * @feature
 */
fixture(`Feature: klaxoon storm check words`)
  .before(async (ctx) => {
    // inject global configuration in the fixture context
    ctx.config = getCurrentConfig();
  })
  .beforeEach(async (t) => {
    // inject page model in the test context
    t.ctx.inputData = pageModel;
  });

test('Scenario: acces to storm', async () => {
  await given('I navigate to klaxoon');
  await when('I send an access code');
  await and('I send a pseudo');
  await then('I see the good stormname');
  await when('I open livestorm');
  await then('I see the good livestorm');
  await when('I open ideas');
  await then('I see expected words');
});
